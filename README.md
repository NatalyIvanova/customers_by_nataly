#View in live

https://mui-theming-by-nataly.netlify.app

# Technology Stack 

"react": "18.2.0",
@mui/material": "5.10.2,
"styled-components": "5.3.5",
"typescript": "4.7.4",
"@craco/craco": "6.4.5"


# Main features

MUI + React + Typescript

MUI's style engine by default (emotion) is replaced by styled-components

MUI base theming + 2 custom themes

Dynamic themes' toggling (click on floating switch button)

MUI components' adoption according to custom requirements (header is over the sidebar)

Responsive on every screen, including tablet and mobile



