import React, { useState, useEffect } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { customTheme } from "./theme/customTheme";
import { customThemeSouth } from "./theme/customThemeSouth";
import { customThemeWest } from "./theme/customThemeWest";
import MainContainer from "./components/mainContainer";
import { defaultValues } from './assets/data';
import { themes } from "./enums/themeEnum";

const customThemes = {
    default: customTheme,
    south: customThemeSouth,
    west: customThemeWest,
}

export default function App() {
    const themeName = process?.env.REACT_APP_THEME || null;
    const [currentThemeName, setCurrentThemeName] = useState<themes>(themes.Default);

    useEffect(() => {
        if (themeName && themeName !== themes.Default) setTheme(themeName as themes);
    }, [themeName])

    const setTheme = (themeName: themes): void => {
        setCurrentThemeName(themeName);
    }

    return (
        <ThemeProvider theme={customThemes[currentThemeName]}>
            <MainContainer userName={defaultValues.name} themeName={currentThemeName} onThemeChange={setTheme}/>
        </ThemeProvider>
    );
}
