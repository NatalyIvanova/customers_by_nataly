import { createTheme } from '@mui/material/styles';

export let customTheme = createTheme({
	colors: {
		common: {
			white: '#ffffff',
			black: '#03030B',
			border: '#E6E8F0',
			error: '#D14343',
		}
	}
});

customTheme = createTheme(customTheme, {
	typography: {
		htmlFontSize: 16,
		h1: {
			fontSize: 20,
			fontWeight: 500,
			textTransform: 'uppercase',
			letterSpacing: 0,
			color: customTheme.colors.common.white,
			lineHeight: 1,
			margin: 0
		},
		h2: {
			fontSize: 20,
			fontWeight: 400,
			letterSpacing: 0,
			color: customTheme.colors.common.black,
			lineHeight: 1,
			margin: 0
		},
		body1: {
			fontSize: 16
		},
		body2: {
			fontSize: 13,
			marginTop: 0,
			marginBottom: 0
		}
	},
	variables: {
		drawerWidth: 240,
		drawerSmallWidth: 56,
		topBarHeight: 64
	},
	pattern: {
		flex: {
			display: 'flex',
			alignItems: 'center'
		},
		ellipsis: {
			whiteSpace: 'nowrap',
			overflow: 'hidden',
			textOverflow: 'ellipsis'
		},
		border: `1px solid ${customTheme.colors.common.border}`,
		scrollBar: {
			'&::-webkit-scrollbar': {
				width: 8,
				borderRadius: 0,
				backgroundColor: 'transparent',
				border: 0,
				'&-track': {
					backgroundColor: 'transparent'
				},
				'&-thumb': {
					backgroundColor: customTheme.colors.common.border,
					borderRadius: 0,
					borderRight: `4px solid ${customTheme.palette.common.white}`
				}
			}
		}
	}
});
