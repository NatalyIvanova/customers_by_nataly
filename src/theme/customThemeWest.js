import { createTheme } from '@mui/material/styles';
import { customTheme } from "./customTheme";

export const customThemeWest = createTheme(customTheme, {
	palette: {
		primary: {
			main: '#ed6c02',
			light: '#ff9800',
			dark: '#e65100'
		}
	}
});
