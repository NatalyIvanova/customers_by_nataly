import { createTheme } from '@mui/material/styles';
import { customTheme } from "./customTheme";

export const customThemeSouth = createTheme(customTheme, {
	palette: {
		primary: {
			main: '#0288d1',
			light: '#03a9f4',
			dark: '#01579b'
		}
	}
});
