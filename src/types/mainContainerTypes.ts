import { eMuiIconNames } from '../components/elements/iconComponent';

export interface MenuItem {
    name: string;
    iconName: eMuiIconNames;
}

export interface Customer {
    name: string;
    date: string;
    phone: string;
    location: string;
}

