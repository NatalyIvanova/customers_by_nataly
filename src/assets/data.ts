import { MenuItem, Customer } from '../types/mainContainerTypes';
import { eMuiIconNames } from '../components/elements/iconComponent';

export const defaultValues = {
    name: 'Renee McLelvey'
};

export const menuList: MenuItem[] = [
	{name: 'Edit', iconName: eMuiIconNames.Edit},
	{name: 'Location', iconName: eMuiIconNames.Location},
	{name: 'Organisation', iconName: eMuiIconNames.Organisation},
	{name: 'Profile', iconName: eMuiIconNames.Profile}
];

export const customersList: Customer[] = [
	{
		name: 'Jerry Mattedi',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Elianora Vasilov',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Marcos Anguliano',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Alvis Daen',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Jerry Mattedi',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Elianora Vasilov',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Marcos Anguliano',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
	{
		name: 'Alvis Daen',
		date: '13 aug 2018',
		phone: '251-661-5362',
		location: 'New York'
	},
    {
        name: 'Jerry Mattedi',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Elianora Vasilov',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Marcos Anguliano',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Alvis Daen',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Jerry Mattedi',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Elianora Vasilov',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Marcos Anguliano',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    },
    {
        name: 'Alvis Daen',
        date: '13 aug 2018',
        phone: '251-661-5362',
        location: 'New York'
    }
];