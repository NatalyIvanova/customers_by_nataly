import React, { useState } from 'react';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {
    HeaderSt,
    MenuIconButton,
    DrawerHeader,
    AppBar,
    Drawer,
    SubheaderSt,
    AccountCircleIconSt,
    MainSt
} from './mainContainerStyled';
import MenuListComponent from './menuList';
import CustomersListComponent from './customersList';
import { MenuItem } from '../types/mainContainerTypes';
import { menuList, customersList } from '../assets/data';
import { themes } from "../enums/themeEnum";
import ThemeSwitch from "./themeSwitch";

interface MainContainerProps {
    userName: string;
    themeName: themes;
    onThemeChange: (themeName: themes) => void;
}

const MainContainer = ({userName, themeName, onThemeChange}: MainContainerProps) => {
    const compName = 'main-container';
    const [open, setOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState<MenuItem | null>(null);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleSelectedMenuItem = (item: MenuItem) => {
        setSelectedItem(item);
    };

    const handleThemeChange = (event: React.ChangeEvent<HTMLInputElement>, value: themes) => {
        onThemeChange(value);
    };

    return (
        <div className={compName}>
            <AppBar className={`${compName}-topbar`} position="fixed" open={open}>
                <MenuIconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    open={open}
                    className={`${compName}-toogle`}>
                    <MenuIcon />
                </MenuIconButton>
                <div className={`${compName}-title`}>
                    <HeaderSt>{userName}</HeaderSt>
                    <SubheaderSt>{selectedItem ? selectedItem.name : 'Account Settings'}</SubheaderSt>
                </div>
                <AccountCircleIconSt/>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <DrawerHeader className={`${compName}-drawer-header`}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <MenuListComponent menuList={menuList} onSelected={handleSelectedMenuItem} open={open}/>
            </Drawer>
            <MainSt className={`${compName}-main`}>
                <CustomersListComponent customersList={customersList}/>
            </MainSt>
            <ThemeSwitch themeName={themeName} onThemeChange={handleThemeChange}/>
        </div>
    );
};

export default MainContainer;
