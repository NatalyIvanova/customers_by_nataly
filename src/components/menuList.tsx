import React from 'react';
import styled from "styled-components";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton, { ListItemButtonProps } from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import IconComponent from './elements/iconComponent';
import { MenuItem } from '../types/mainContainerTypes';

interface MenuListProps {
    menuList: MenuItem[];
    onSelected: (item: MenuItem) => void;
    open?: boolean;
}

interface ListItemButtonStProps extends ListItemButtonProps {
    open?: boolean;
}

const ListItemSt = styled(ListItem)({
    display: 'block'
});

const ListItemButtonSt = styled(ListItemButton)<ListItemButtonStProps>(
    ({ theme, open}) => ({
        minHeight: 48,
        justifyContent: open ? 'initial' : 'center',
    })
);

const ListItemIconSt = styled(ListItemIcon)<ListItemButtonStProps>(
    ({ theme, open}) => ({
        minWidth: 0,
        marginTop: theme.spacing(1 / 2),
        marginRight: open ? theme.spacing(3) : theme.spacing(1 / 2),
        marginBottom: theme.spacing(1 / 2),
        marginLeft: theme.spacing(1 / 2),
        justifyContent: 'center'
    })
);


const MenuListComponent = ({menuList, onSelected, open}: MenuListProps) => {
    const compName = 'menu-list';
    return (
        <List className={compName}>
            {menuList.map((item, index) => (
                <ListItemSt
                    key={item.name}
                    disablePadding
                    onClick={() => onSelected(item)}
                    className={`${compName}-item`}>
                    <ListItemButtonSt open={open} className={`${compName}-button`}>
                        <ListItemIconSt open={open} className={`${compName}-icon`}>
                            <IconComponent iconName={item.iconName}/>
                        </ListItemIconSt>
                        <ListItemText primary={item.name} sx={{ opacity: open ? 1 : 0 }} />
                    </ListItemButtonSt>
                </ListItemSt>
            ))}
        </List>
    )
};

export default MenuListComponent;