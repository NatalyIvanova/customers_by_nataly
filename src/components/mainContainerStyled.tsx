import styled from 'styled-components';
import MuiDrawer, { DrawerProps } from '@mui/material/Drawer';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { IconButton } from "@mui/material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { devices } from '../theme/breakpoints';

const transition = (theme) => ({
    transition: 'width 280ms ease-in-out',
    overflowX: 'hidden',
});

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}

export const AppBar = styled(MuiAppBar)<AppBarProps>`
    ${({ theme, open}) => ({
        ...theme.pattern.flex,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 3),
        ...theme.mixins.toolbar,
        zIndex: theme.zIndex.drawer + 1,
        ...transition(theme),
        ...(open && {
            marginLeft: theme.variables.drawerWidth,
            width: `calc(100% - ${theme.variables.drawerWidth}px)`,
        }),
    })}
    @media ${devices.mobileM} {
        ${({ theme, open }) => ({
            width: '100%',
            transition: 'margin-right 280ms',
            ...(open && {
                marginRight: '-100%',
            })
        })}
    }
`;

interface StyledDrawer extends DrawerProps {
    open?: boolean;
}

export const Drawer = styled(MuiDrawer)<StyledDrawer>`
    ${({ theme, open}) => ({
        width: theme.variables.drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        transition: 'width 280ms ease-in-out',
        overflowX: 'hidden',
        ...(open && {
            width: theme.variables.drawerWidth,
            '& .MuiDrawer-paper': {
                width: theme.variables.drawerWidth,
                transition: 'width 280ms ease-in-out',
                overflowX: 'hidden',
            },
        }),
        ...(!open && {
            width: theme.variables.drawerSmallWidth + 1,
            '& .MuiDrawer-paper': {
                width: theme.variables.drawerSmallWidth + 1,
                transition: 'width 280ms ease-in-out',
                overflowX: 'hidden',
            }
        })
    })}
    @media ${devices.mobileM} {
        ${({open}) => ({
            ...(open && {
                width: '100%',
                '& .MuiDrawer-paper': {
                    width: '100%'
                },
            })
        })}
    }
`;

export const DrawerHeader = styled('div')(({ theme }) => ({
    ...theme.pattern.flex,
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
}));

export const HeaderSt = styled('h1')(({ theme }) => ({
    ...theme.typography.h1
}));

export const SubheaderSt = styled('p')(({ theme }) => ({
    textAlign: 'right',
    ...theme.typography.body2
}));

export const AccountCircleIconSt = styled(AccountCircleIcon)(({ theme }) => ({
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginLeft: theme.spacing(2)
}));

export const MenuIconButton = styled(IconButton)<AppBarProps>`
    ${({ theme, open}) => ({
        marginRight: 'auto',
        ...(open && {
            display: 'none'
        }),
    })}
`;

export const MainSt = styled('main')(({ theme }) => ({
    height: '100vh',
    flexGrow: 1,
    paddingTop: `calc(${theme.variables.topBarHeight}px + ${theme.spacing(4)})`,
    paddingRight: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    paddingLeft: `calc(${theme.variables.drawerSmallWidth}px + ${theme.spacing(4)})`,
    overflowX: 'hidden',
    overflowY: 'hidden',
    boxSizing: 'border-box'
}));