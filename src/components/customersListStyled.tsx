import styled from "styled-components";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { devices } from '../theme/breakpoints';

export const ListSt = styled(List)(
    ({ theme }) => ({
        width: '100%',
        maxHeight: '100%',
        border: `1px solid ${theme.colors.common.border}`,
        padding: 0,
        overflowY: 'auto',
        ...theme.pattern.scrollBar
    })
);

export const ListItemSt = styled(ListItem)(
    ({ theme }) => ({
        ...theme.pattern.flex,
        justifyContent: 'stretch',
        ...theme.typography.body1,
        height: theme.spacing(6),
        '&:nth-child(even)': {
            backgroundColor: theme.palette.grey[100]
        },
        [`@media ${devices.tablet}`]: {
            flexWrap: 'wrap',
            justifyContent: 'center',
            height: 'auto'
        }
    })
);

export const CellSt = styled('div')(
    ({ theme }) => ({
        padding: theme.spacing(0, 2),
        flexGrow: 0,
        flexBasis: `calc((100% / 6) - ${theme.spacing(2)})`,
        '&.name': {
            flexGrow: 1
        },
        '&.button': {
            flexBasis: 0
        },
        [`@media ${devices.tablet}`]: {
            textAlign: 'center',
            padding: theme.spacing(1, 2),
            flexBasis: '100%'
        }
    })
);
