import React from 'react';
import Button from '@mui/material/Button';
import { Customer } from '../types/mainContainerTypes';
import { ListSt, ListItemSt, CellSt } from './customersListStyled';

interface CustomersListProps {
    customersList: Customer[];
}

const CustomersListComponent = ({customersList}: CustomersListProps) => {
    const compName = 'customers-list';

    return (
        <ListSt className={compName}>
            {customersList.map((item, index) => (
                <ListItemSt
                    key={item.name + index}
                    disablePadding
                    className={`${compName}-item`}>
                    {Object.entries(item).map(([key, value]) => (
                        <CellSt className={`${compName}-cell ${key}`} key={key}>
                            {value}
                        </CellSt>
                    ))}
                    <CellSt className={`${compName}-cell button details`}>
                        <Button variant="contained" color="secondary" size="small">Options</Button>
                    </CellSt>
                    <CellSt className={`${compName}-cell button options`}>
                        <Button variant="contained" color="primary" size="small">Details</Button>
                    </CellSt>
                </ListItemSt>
            ))}
        </ListSt>
    )
};

export default CustomersListComponent;