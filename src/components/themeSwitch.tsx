import React from 'react';
import Switch from '@mui/material/Switch';
import { themes } from "../enums/themeEnum";
import styled from "styled-components";

interface ThemeSwitchProps {
    themeName: themes;
    onThemeChange: (event, value) => void;
}

export const FloatingSwitch = styled('div')(
    ({ theme }) => ({
        display: 'grid',
        placeItems: 'center',
        width: '50px',
        height: '50px',
        backgroundColor: theme.palette.background.paper,
        boxShadow: `3px 3px 20.5px 3px ${theme.palette.primary.main}`,
        borderRadius: '50%',
        position: 'fixed',
        right: '30px',
        bottom: '30px'
    })
);

const ThemeSwitch = ({themeName, onThemeChange}: ThemeSwitchProps) => {
    const compName = 'theme-switch';
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>, value: Boolean) => {
        onThemeChange(event, value ? themes.South : themes.West);
    };

    return (
        <FloatingSwitch className={compName}>
            <Switch
                checked={themeName === themes.South}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
                size="small"
            />
        </FloatingSwitch>
    );
}

export default ThemeSwitch;