import React from 'react';
import * as MuiIcons from '@mui/icons-material';
import { DefaultComponentProps } from '@mui/material/OverridableComponent';
import { SvgIconTypeMap } from '@mui/material';

// in abc order
export enum eMuiIconNames {
    Edit = 'EditOutlined',
    Location = 'PlaceOutlined',
    Organisation = 'CorporateFare',
    Profile = 'PersonOutline'
}

interface iIconComponentProps extends DefaultComponentProps<SvgIconTypeMap> {
    iconName: eMuiIconNames;
    customClass?: string;
    customStyles?: Record<string, unknown>;
}

const IconComponent = (props: iIconComponentProps) => {
    const Icon = MuiIcons[props.iconName];
    return <Icon fontSize={props.fontSize} className={props.customClass} sx={props.customStyles}/>;
};

export default IconComponent;